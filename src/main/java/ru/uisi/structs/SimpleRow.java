package ru.uisi.structs;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author andrey
 * @since 10/11/13 - 8:06 PM
 */
public abstract class SimpleRow
{
    private int id;
    @NotEmpty(message = "Заполните поля!")
    private String name;

    public SimpleRow()
    {
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
