package ru.uisi.structs;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

/**
 * @author andrey
 * @since 11/3/13 - 3:47 PM
 */
public class Advertisement extends SimpleRow
{
    private String address;
    private int category;
    private int city;
    private Date date;
    @Email(message = "Введите корректный email")
    private String email;
    @Length(min = 5)
    private String phone;
    private int price;
    private String text;
    private int views;

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public int getCategory()
    {
        return category;
    }

    public void setCategory(int category)
    {
        this.category = category;
    }

    public int getCity()
    {
        return city;
    }

    public void setCity(int city)
    {
        this.city = city;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public int getViews()
    {
        return views;
    }

    public void setViews(int views)
    {
        this.views = views;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder(getId());
        sb.append(":").append(getName());
        return sb.toString();
    }
}
