package ru.uisi;

/**
 * Вспомогательные методы приложения
 *
 * @author andrey
 * @since 10/29/13 - 9:34 PM
 */
public class Utils
{
    /**
     * Вычисление оффсета для пейджинга страниц
     *
     * @param page
     * @return
     */
    public static int calculateOffset(String page)
    {
        if (Utils.isEmptyTrim(page))
        {
            return 0;
        }
        return (Integer.valueOf(page) - 1) * 10;
    }

    /**
     * Проверяет, содержит ли строка null или только whitespace символы
     *
     * @param string
     * @return
     */
    public static boolean isEmptyTrim(String string)
    {
        return string == null || string.trim().equals("");
    }
}
