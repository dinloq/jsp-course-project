package ru.uisi;

/**
 * Константы приложения, по сути повторно используемые, кэшируемые строки
 *
 * @author andrey
 * @since 10/11/13 - 8:25 PM
 */
public class Constants
{
    public static interface ConnectorConstants
    {
        int PORT = 5432;
        String DB_NAME = "uisi";
        String ADDRESS = "localhost";
        String ORG_POSTGRESQL_DRIVER = "org.postgresql.Driver";
        String DB_URL = "jdbc:postgresql://" + ADDRESS + ":" + PORT + "/" + DB_NAME + "?useUnicode=true&characterEncoding=UTF-8";
        String USER = "stand";
        String PASSWORD = "stand";
    }

    public static interface FieldConstants
    {
        String ID_FIELD = "id";
        String NAME_FIELD = "name";
        String EMAIL = "email";
        String DATE = "date";
        String TEXT_FIELD = "text";
        String PRICE = "price";
        String PHONE = "phone";
        String ADDRESS = "address";
        String CITY = "city";
        String CAT = "cat";
        String VIEWS = "views";
        String AD = "ad";
        String CATEGORY = "category";
        String ADVERTISEMENT = "advertisement";
    }

    public static interface TablesConstants
    {
        String ADS = "ads";
        String CATEGORIES = "categories";
        String CITIES = "cities";
    }

    public static interface ControllersConstants
    {
        String MODE = "mode";
        String ADD = "add";
        String INDEX_PAGE = "index";
        String VIEW = "view";
        String LIST = "list";
        String CATEGORIES_PANEL_PAGE = "widget/categories-panel";
        String ADMIN_LIST_MODE = "adminList";
        String ADMIN_UPDATE = "adminUpdate";
        String TOP_LIST = "widget/top/top-list";
    }

    public static interface AddItemConstants
    {
        String ADD_ITEM_STATUS_PROP = "addItemStatus";
        String PROGRESS = "progress";
        String SUCCESS = "success";
        String ADDED_ITEM_ID_PROP = "addedItem";
        String ADVERTISEMENT_PROP = "ADVERTISEMENT";
    }

    public static interface MappingConstants
    {
        String ADD_ITEM_MAPPING = "/add-item";
        String INDEX_MAPPING = "/index";
        String VIEW_MAPPING = "/view";
        String BASE_MAPPING = "/";
        String CATEGORIES_PANEL_MAPPING = "/categories-panel";
        String ADMIN_BASE_MAPPING = "/admin";
        String LIST_ADMIN_MAPPING = "/list";
        String DELETE_MAPPING = "/delete";
        String UPDATE_MAPPING = "/update";
        String TOP_LIST_MAPPING = "/top-list";
    }

    public static interface ParametersConstants
    {
        String CAT_PARAM = "cat";
        String ID_PARAM = "id";
        String ADS_PARAM = "ads";
        String CATEGORIES_PARAM = "categories";
        String CITIES_PARAM = "cities";
        String TOPS_PARAM = "tops";
        String HAS_NEXT_PAGE_PARAM = "hasNextPage";
        String PAGE_INDEX_PARAM = "pageIndex";
    }

    public static interface LoggerConstants
    {
        String EXECUTING_QUERY = "Executing query:";
        String CREATING_USER_FAILED_NO_ROWS_AFFECTED = "Creating user failed, no rows affected.";
    }
}
