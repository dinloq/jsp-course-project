package ru.uisi.controller;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.uisi.beans.DAO.AdsDao;
import ru.uisi.beans.JspUtils;
import ru.uisi.structs.Advertisement;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.Date;

import static ru.uisi.Constants.AddItemConstants.*;
import static ru.uisi.Constants.ControllersConstants.*;
import static ru.uisi.Constants.FieldConstants.ADVERTISEMENT;
import static ru.uisi.Constants.FieldConstants.DATE;
import static ru.uisi.Constants.MappingConstants.ADD_ITEM_MAPPING;
import static ru.uisi.Constants.TablesConstants.CATEGORIES;
import static ru.uisi.Constants.TablesConstants.CITIES;


/**
 * Контроллер добавления объявления
 *
 * @author andrey
 * @since 11/3/13 - 1:26 PM
 */
@Controller
@RequestMapping(value = ADD_ITEM_MAPPING)
public class AddAdvertisementController
{
    private static final Log logger = LogFactory.getLog(AddAdvertisementController.class);
    @Inject
    JspUtils jspUtils;
    @Inject
    AdsDao adsDao;

    /**
     * Отвечает за процессинг формы
     *
     * @param advertisement
     * @param result
     * @param model
     * @return
     */
    @RequestMapping( method = RequestMethod.POST )
    public String processForm(@ModelAttribute( value = ADVERTISEMENT ) Advertisement advertisement, BindingResult result, ModelMap model)
    {
        model.addAttribute(MODE, ADD);
        if (result.hasErrors())
        {
            model.addAttribute(ADD_ITEM_STATUS_PROP, PROGRESS);
            return INDEX_PAGE;
        }
        else
        {
            model.addAttribute(ADD_ITEM_STATUS_PROP, SUCCESS);
            try
            {
                int id = adsDao.insert(advertisement);
                model.addAttribute(ADDED_ITEM_ID_PROP, id);
            }
            catch (SQLException e)
            {
                logger.error(e.getMessage(), e);
            }
            return INDEX_PAGE;
        }
    }

    /**
     * Отвечает за показ формы и ее значений
     *
     * @param model
     * @return
     */
    @RequestMapping( method = RequestMethod.GET )
    public String showForm(ModelMap model)
    {
        model.addAttribute(ADD_ITEM_STATUS_PROP, PROGRESS);
        initController(model);
        return INDEX_PAGE;
    }

    private void initController(ModelMap model)
    {
        Advertisement ad = new Advertisement();
        model.addAttribute(ADVERTISEMENT, ad);
        model.addAttribute(MODE, ADD);
        model.addAttribute(CATEGORIES, jspUtils.getMapCategories());
        model.addAttribute(CITIES, jspUtils.getMapCities());
        model.addAttribute(DATE, new Date());
    }
}
