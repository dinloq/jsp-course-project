package ru.uisi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController
{
    @RequestMapping(value = "/login")
    public String login(ModelMap model)
    {
        return "admin/security/login";
    }

    @RequestMapping(value = "/loginfailed")
    public String loginError(ModelMap model)
    {
        model.addAttribute("error", "true");
        return "admin/security/login";
    }

    @RequestMapping(value = "/logout")
    public String logout(ModelMap model)
    {
        return "admin/security/login";
    }
}
