package ru.uisi.controller;

import com.google.common.collect.Lists;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.uisi.beans.DAO.AdsDao;
import ru.uisi.beans.DAO.CategoryDao;
import ru.uisi.beans.DAO.CityDao;
import ru.uisi.beans.JspUtils;
import ru.uisi.beans.SqlQueryFormatter;
import ru.uisi.structs.Advertisement;
import ru.uisi.structs.Category;
import ru.uisi.structs.City;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static ru.uisi.Constants.AddItemConstants.ADVERTISEMENT_PROP;
import static ru.uisi.Constants.ControllersConstants.*;
import static ru.uisi.Constants.FieldConstants.*;
import static ru.uisi.Constants.MappingConstants.*;
import static ru.uisi.Constants.ParametersConstants.*;
import static ru.uisi.Constants.TablesConstants.*;

/**
 * Контроллер действий администратора
 *
 * @author andrey
 * @since 11/4/13 - 12:20 AM
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController
{
    public static final String SUCCESS = "success";
    public static final String UPDATE_STATUS = "updateStatus";
    private static final Log logger = LogFactory.getLog(AdminController.class);
    @Inject
    AdsDao adsDao;
    @Inject
    SqlQueryFormatter formatter;
    @Inject
    CategoryDao categoryDao;
    @Inject
    CityDao cityDao;
    @Inject
    JspUtils jspUtils;

    /**
     * Выполнение операции удаления
     *
     * @param id    - id удаляемой страницы
     * @param model - модель
     * @return jsp page
     * @throws SQLException
     */
    @RequestMapping(value = DELETE_MAPPING, method = RequestMethod.GET)
    public String deleteAdmin(@RequestParam(value = ID_PARAM) String id, ModelMap model) throws SQLException
    {
        adsDao.deleteById(id);
        return showListAdmin(model);
    }

    /**
     * Вывод списка объявлений в режиме администратора
     *
     * @param model - spring model
     * @return jsp page
     * @throws SQLException
     */
    @RequestMapping(value = LIST_ADMIN_MAPPING)
    public String showListAdmin(ModelMap model) throws SQLException
    {
        model.addAttribute(MODE, ADMIN_LIST_MODE);
        getListData(model);
        return INDEX_PAGE;
    }

    /**
     * Обновление объявления администратором
     *
     * @param advertisement - обновляемое объявления
     * @param model         - spring model
     * @return jsp page
     */
    @RequestMapping(value = UPDATE_MAPPING)
    public String showUpdateItemForm(@ModelAttribute(value = ADVERTISEMENT) Advertisement advertisement, ModelMap model)
    {
        model.addAttribute(MODE, ADMIN_UPDATE);
        adsDao.update(advertisement);
        model.addAttribute(UPDATE_STATUS, SUCCESS);
        model.addAttribute(ID_FIELD, advertisement.getId());
        return INDEX_PAGE;
    }

    /**
     * Просмотр объявления
     *
     * @param advertisement
     * @param id
     * @param model
     * @return
     * @throws SQLException
     */
    @RequestMapping(value = VIEW_MAPPING)
    public String updateItemAdmin(@ModelAttribute(value = ADVERTISEMENT_PROP) Advertisement advertisement, @RequestParam(value = ID_PARAM) String id, ModelMap model)
    {
        model.addAttribute(MODE, ADMIN_UPDATE);
        try
        {
            prepareController(id, model);
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage(), e);
        }
        return INDEX_PAGE;
    }

    private void getListData(ModelMap model) throws SQLException
    {
        Map<Integer, Advertisement> map = adsDao.getMap(formatter.selectSort(ADS_PARAM, DATE, false));
        List<Advertisement> ads = Lists.newLinkedList(map.values());
        Map<Integer, Category> categories = categoryDao.getMap(formatter.select(CATEGORIES));
        Map<Integer, City> cities = cityDao.getMap(formatter.select(CITIES));

        model.addAttribute(ADS_PARAM, ads);
        model.addAttribute(CATEGORIES_PARAM, categories);
        model.addAttribute(CITIES_PARAM, cities);
    }

    private Advertisement prepareController(String id, ModelMap model) throws SQLException
    {
        Advertisement ad = adsDao.getFirstResult(formatter.selectWhereId(ADS, id));
        model.addAttribute(ADVERTISEMENT_PROP, ad);
        model.addAttribute(ID_FIELD, ad.getId());
        model.addAttribute(CATEGORIES, jspUtils.getMapCategories());
        model.addAttribute(CITIES, jspUtils.getMapCities());
        return ad;
    }
}
