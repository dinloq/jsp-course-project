package ru.uisi.controller;

import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.uisi.Constants;
import ru.uisi.Utils;
import ru.uisi.beans.DAO.AdsDao;
import ru.uisi.beans.DAO.CategoryDao;
import ru.uisi.beans.DAO.CityDao;
import ru.uisi.beans.SqlQueryFormatter;
import ru.uisi.structs.Advertisement;
import ru.uisi.structs.Category;
import ru.uisi.structs.City;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static ru.uisi.Constants.ControllersConstants.*;
import static ru.uisi.Constants.FieldConstants.*;
import static ru.uisi.Constants.MappingConstants.*;
import static ru.uisi.Constants.ParametersConstants.*;
import static ru.uisi.Constants.TablesConstants.*;
import static ru.uisi.beans.SqlQueryFormatter.CAT_EQ;

/**
 * Контроллер сервисов, таких, как получение отдельного объекта, либо списка
 *
 * @author andrey
 * @since 10/29/13 - 8:12 PM
 */
@Controller
@RequestMapping( BASE_MAPPING )
public class ServiceController
{
    public static final Log logger = LogFactory.getLog(ServiceController.class);
    @Inject
    AdsDao adsDao;
    @Inject
    CategoryDao categoryDao;
    @Inject
    SqlQueryFormatter formatter;
    @Inject
    CityDao cityDao;

    /**
     * Сервис просмотра списка объектов с*(или без) запрашиваемой категории
     *
     * @param model
     * @param cat   - необязательный параметр, id категории
     * @return
     */
    @RequestMapping( value = INDEX_MAPPING, method = RequestMethod.GET )
    public String adsList(ModelMap model, @RequestParam( value = CAT, required = false ) String cat, @RequestParam( value = "page", required = false ) String page) throws SQLException
    {
        model.addAttribute(MODE, LIST);

        getViewListMetainfo(model, cat, page);
        return INDEX_PAGE;
    }

    /**
     * Сервис получения имен и id категорий
     *
     * @param model
     * @return
     * @throws SQLException
     */
    @RequestMapping( value = CATEGORIES_PANEL_MAPPING )
    public String getCategoriesNames(ModelMap model) throws SQLException
    {
        String query = formatter.select(CATEGORIES);
        List<Category> rows = Lists.newLinkedList(categoryDao.getMap(query).values());
        model.addAttribute(CATEGORIES, rows);
        return CATEGORIES_PANEL_PAGE;
    }

    /**
     * Контроллер загрузки самых просматриваемых страниц
     *
     * @param model
     * @return
     * @throws SQLException
     */
    @RequestMapping( value = TOP_LIST_MAPPING )
    public String topList(ModelMap model) throws SQLException
    {
        String query = formatter.selectSortLimit(ADS, VIEWS, false, 5);
        List<Advertisement> list = Lists.newLinkedList(adsDao.getMap(query).values());
        model.addAttribute(Constants.ParametersConstants.TOPS_PARAM, list);
        return TOP_LIST;
    }

    /**
     * Сервис просмотра отдельного объявления
     *
     * @param model
     * @param id    - id объявления
     * @return
     */
    @RequestMapping( value = VIEW_MAPPING, method = RequestMethod.GET )
    public String viewItem(ModelMap model, @RequestParam( value = ID_PARAM ) String id) throws SQLException
    {
        model.addAttribute(MODE, VIEW);

        getViewItemMetainfo(model, id);
        return INDEX_PAGE;
    }

    private void getViewItemMetainfo(ModelMap model, String id) throws SQLException
    {
        String query = formatter.selectWhereId(ADS, id);
        Advertisement row = adsDao.getFirstResult(query);
        City city = cityDao.getFirstResult(formatter.selectWhereId(CITIES, row.getCity()));
        Category category = categoryDao.getFirstResult(formatter.selectWhereId(CATEGORIES, row.getCategory()));

        adsDao.incrementField(VIEWS, row.getId());
        model.addAttribute(AD, row);
        model.addAttribute(CITY, city);
        model.addAttribute(CATEGORY, category);
    }

    private void getViewListMetainfo(ModelMap model, String cat, String page) throws SQLException
    {
        Range<Integer> catRange = Range.closed(1, 5);
        int category = (Utils.isEmptyTrim(cat)) ? 0 : Integer.valueOf(cat);

        /**
         * Формировани запроса к БД для получения коллекции элементов, в зависимости
         * от категории и текущей страницы
         */
        //@formatter:off
        String query = (!(catRange.apply(category))
                ? formatter.selectSort(ADS, DATE, false)
                : formatter.selectWhereSort(ADS, CAT_EQ + cat, DATE, false))
                + formatter.limitOffset(11, Utils.calculateOffset(page));
        //@formatter:on
        logger.info(query);
        Map<Integer, Category> categories = categoryDao.getMap(formatter.select(CATEGORIES));

        List<Advertisement> rows = Lists.newLinkedList(adsDao.getMap(query).values());
        model.addAttribute(CAT_PARAM, category);
        model.addAttribute(ADS_PARAM, rows);
        model.addAttribute(CATEGORIES_PARAM, categories);
        model.addAttribute(HAS_NEXT_PAGE_PARAM, rows.size() > 10);
        model.addAttribute(PAGE_INDEX_PARAM, Utils.isEmptyTrim(page) ? 1 : Integer.valueOf(page));
    }
}
