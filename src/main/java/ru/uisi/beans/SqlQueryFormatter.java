package ru.uisi.beans;

import org.springframework.stereotype.Component;
import ru.uisi.Constants;

import static ru.uisi.Constants.FieldConstants.*;
import static ru.uisi.Constants.TablesConstants.ADS;

/**
 * Форматтер для sql запросов
 *
 * @author andrey
 * @since 10/26/13 - 5:03 PM
 */
@Component
public class SqlQueryFormatter
{
    public static final String CAT_EQ = "cat=";
    public static final String COMMA = ",";
    public static final String CLOSE_BRACE = ")";
    public static final String INTO = "INTO";
    public static final String INSERT = "INSERT";
    public static final String OPEN_BRACE = "(";
    public static final String VALUES = "VALUES";
    public static final String QUEST_SYMBOL = "?";
    public static final String DELETE = "DELETE";
    public static final String EQ_SYMBOL = "=";
    public static final String SET = "SET";
    public static final String UPDATE = "UPDATE";
    public static final String INC = "+1";
    public static final String LIMIT = "LIMIT";
    public static final String OFFSET = "OFFSET";
    public static final String BEGIN_SEARCH = "'%";
    public static final String END_SEARCH = "%'";
    public static final String LIKE = "LIKE";
    private static final String ASC = "ASC";
    private static final String DESC = "DESC";
    private static final String EVERYTHING = "*";
    private static final String FROM = "FROM";
    private static final String ID_EQ = "id=";
    private static final String ORDER_BY = "ORDER BY";
    private static final String SELECT = "SELECT";
    private static final String SPACE = " ";
    private static final String WHERE = "WHERE";

    public String deleteById(String from)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(DELETE).append(SPACE).append(FROM).append(SPACE);
        sb.append(from).append(SPACE).append(WHERE).append(SPACE);
        sb.append(ID_EQ).append(QUEST_SYMBOL);
        return sb.toString();
    }

    public String incrementFieldQuery(String table, String field)
    {
        StringBuilder sb = new StringBuilder(UPDATE);
        sb.append(SPACE).append(table).append(SPACE);
        sb.append(SET).append(SPACE);
        sb.append(field).append(EQ_SYMBOL).append(field).append(INC).append(SPACE);
        sb.append(WHERE).append(SPACE).append(ID_EQ).append(QUEST_SYMBOL);
        return sb.toString();
    }

    public String insertAdvertisement()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(INSERT).append(SPACE).append(INTO).append(SPACE);
        sb.append(ADS).append(SPACE).append(OPEN_BRACE);
        sb.append(NAME_FIELD).append(COMMA).append(SPACE);
        sb.append(CAT).append(COMMA).append(SPACE);
        sb.append(ADDRESS).append(COMMA).append(SPACE);
        sb.append(DATE).append(COMMA).append(SPACE);
        sb.append(PHONE).append(COMMA).append(SPACE);
        sb.append(PRICE).append(COMMA).append(SPACE);
        sb.append(TEXT_FIELD).append(COMMA).append(SPACE);
        sb.append(CITY).append(COMMA).append(SPACE);
        sb.append(EMAIL).append(CLOSE_BRACE).append(SPACE);
        sb.append(VALUES).append(SPACE).append(OPEN_BRACE);
        for (int i = 0; i < 8; i++)
        {
            sb.append(QUEST_SYMBOL).append(COMMA).append(SPACE);
        }
        sb.append(QUEST_SYMBOL).append(CLOSE_BRACE);
        return sb.toString();
    }

    public String limitOffset(int limit, int offset)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(SPACE).append(LIMIT).append(SPACE).append(limit);
        sb.append(SPACE).append(OFFSET).append(SPACE).append(offset);
        return sb.toString();
    }

    public String select(String from)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(SELECT).append(SPACE).append(EVERYTHING).append(SPACE).append(FROM).append(SPACE).append(from);
        return sb.toString();
    }

    public String selectLimitOffset(String from, String sort, int limit, int offset)
    {
        StringBuilder sb = new StringBuilder(selectSortLimit(from, sort, false, limit));
        sb.append(SPACE).append(OFFSET).append(SPACE).append(offset);
        return sb.toString();
    }

    public String selectLimitWhereCatOffset(String from, String sort, int cat, int limit, int offset)
    {
        StringBuilder sb = new StringBuilder(selectWhereSort(from, (CAT_EQ + cat + SPACE), sort, false));
        sb.append(SPACE).append(LIMIT).append(SPACE).append(limit);
        sb.append(SPACE).append(OFFSET).append(SPACE).append(offset);
        return sb.toString();
    }

    public String selectSearchText(String pattern)
    {
        StringBuilder sb = new StringBuilder();
        String whereStatement = SPACE + TEXT_FIELD + SPACE + LIKE + SPACE + BEGIN_SEARCH + pattern + END_SEARCH + SPACE + "OR" + SPACE + NAME_FIELD + SPACE + LIKE + BEGIN_SEARCH + pattern + END_SEARCH + SPACE;
        sb.append(selectWhereSort(ADS, whereStatement, DATE, false));

        return sb.toString();
    }

    public String selectSort(String from, String sortBy, boolean asc)
    {
        StringBuilder sb = new StringBuilder(select(from));
        sb.append(SPACE).append(ORDER_BY).append(SPACE).append(sortBy).append(SPACE);
        return (asc) ? sb.toString() + ASC : sb.toString() + DESC;
    }

    public String selectSortLimit(String from, String sort, boolean asc, int limit)
    {
        StringBuilder sb = new StringBuilder(selectSort(from, sort, asc));
        sb.append(SPACE).append(LIMIT).append(SPACE).append(limit);
        return sb.toString();
    }

    public String selectWhere(String from, String where)
    {
        StringBuilder sb = new StringBuilder(select(from));
        sb.append(SPACE).append(WHERE).append(SPACE).append(where);
        return sb.toString();
    }

    public String selectWhereCat(String from, String cat)
    {
        StringBuilder sb = new StringBuilder(SPACE);
        sb.append(CAT_EQ).append(cat);
        return selectWhere(from, sb.toString());
    }

    public String selectWhereCat(String from, int cat)
    {
        return selectWhereCat(from, String.valueOf(cat));
    }

    public String selectWhereId(String from, int id)
    {
        return selectWhereId(from, String.valueOf(id));
    }

    public String selectWhereId(String from, String id)
    {
        StringBuilder sb = new StringBuilder(SPACE);
        sb.append(ID_EQ).append(id);
        return selectWhere(from, sb.toString());
    }

    public String selectWhereSort(String from, String where, String sortBy, boolean asc)
    {
        StringBuilder sb = new StringBuilder(selectWhere(from, where));
        sb.append(SPACE).append(ORDER_BY).append(SPACE).append(sortBy).append(SPACE);
        return (asc) ? sb.toString() + ASC : sb.toString() + DESC;
    }

    public String updateAdvertisement()
    {
        StringBuilder sb = new StringBuilder(UPDATE);
        sb.append(SPACE).append(Constants.TablesConstants.ADS).append(SPACE);
        sb.append(SET).append(SPACE);
        sb.append(NAME_FIELD).append(EQ_SYMBOL).append(QUEST_SYMBOL).append(COMMA).append(SPACE);
        sb.append(CAT).append(EQ_SYMBOL).append(QUEST_SYMBOL).append(COMMA).append(SPACE);
        sb.append(ADDRESS).append(EQ_SYMBOL).append(QUEST_SYMBOL).append(COMMA).append(SPACE);
        sb.append(PHONE).append(EQ_SYMBOL).append(QUEST_SYMBOL).append(COMMA).append(SPACE);
        sb.append(PRICE).append(EQ_SYMBOL).append(QUEST_SYMBOL).append(COMMA).append(SPACE);
        sb.append(TEXT_FIELD).append(EQ_SYMBOL).append(QUEST_SYMBOL).append(COMMA).append(SPACE);
        sb.append(CITY).append(EQ_SYMBOL).append(QUEST_SYMBOL).append(COMMA).append(SPACE);
        sb.append(VIEWS).append(EQ_SYMBOL).append(QUEST_SYMBOL).append(COMMA).append(SPACE);
        sb.append(EMAIL).append(EQ_SYMBOL).append(QUEST_SYMBOL).append(SPACE);
        sb.append(WHERE).append(SPACE).append(ID_EQ).append(QUEST_SYMBOL);

        return sb.toString();
    }
}
