package ru.uisi.beans.DAO;

import org.springframework.stereotype.Component;
import ru.uisi.Constants;
import ru.uisi.structs.Category;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author andrey
 * @since 10/20/13 - 5:04 PM
 */
@Component
public class CategoryDao extends SelectDao<Category>
{
    @Override
    protected void formatRows(ResultSet resultSet) throws SQLException
    {
        while (resultSet.next())
        {
            Category row = getResultRow(resultSet);
            map.put(row.getId(), row);
        }
    }

    @Override
    protected Category getResultRow(ResultSet resultSet) throws SQLException
    {
        Category row = new Category();
        row.setId(resultSet.getInt(Constants.FieldConstants.ID_FIELD));
        row.setName(resultSet.getString(Constants.FieldConstants.NAME_FIELD));
        return row;
    }
}
