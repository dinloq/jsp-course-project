package ru.uisi.beans.DAO;

import org.springframework.stereotype.Component;
import ru.uisi.Constants;
import ru.uisi.structs.City;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author andrey
 * @since 10/28/13 - 9:02 AM
 */
@Component
public class CityDao extends SelectDao<City>
{
    @Override
    protected void formatRows(ResultSet resultSet) throws SQLException
    {
        while (resultSet.next())
        {
            City row = getResultRow(resultSet);
            map.put(row.getId(), row);
        }
    }

    @Override
    protected City getResultRow(ResultSet resultSet) throws SQLException
    {
        City row = new City();
        row.setId(resultSet.getInt(Constants.FieldConstants.ID_FIELD));
        row.setName(resultSet.getString(Constants.FieldConstants.NAME_FIELD));
        return row;
    }
}
