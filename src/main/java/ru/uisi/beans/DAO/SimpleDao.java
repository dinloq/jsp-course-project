package ru.uisi.beans.DAO;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import ru.uisi.Constants;
import ru.uisi.structs.SimpleRow;

import java.sql.*;

import static ru.uisi.Constants.LoggerConstants.CREATING_USER_FAILED_NO_ROWS_AFFECTED;
import static ru.uisi.Constants.LoggerConstants.EXECUTING_QUERY;

/**
 * @author andrey
 * @since 10/11/13 - 8:28 PM
 */
public abstract class SimpleDao<T extends SimpleRow> extends SelectDao<T>
{
    private final static Log logger = LogFactory.getLog(SimpleDao.class);

    public void deleteById(String id) throws SQLException
    {
        deleteById(Integer.valueOf(id));
    }

    /**
     * Удаление по id из таблицы
     *
     * @param id
     * @throws SQLException
     */
    public void deleteById(int id) throws SQLException
    {
        Connection connection = null;
        PreparedStatement statement = null;
        try
        {
            connection = connectionPool.getConnection();
            String query = getDeleteQuery();
            logger.info(EXECUTING_QUERY + query);
            statement = connection.prepareStatement(query);
            prepareDeleteStatement(id, statement);
            statement.executeUpdate();
        }
        finally
        {
            if (statement != null)
            {
                try
                {
                    statement.close();
                }
                catch (SQLException logOrIgnore)
                {
                    logger.error(logOrIgnore);
                }
            }
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch (SQLException logOrIgnore)
                {
                    logger.error(logOrIgnore);
                }
            }
        }
    }

    public void incrementField(String field, int id)
    {
        Connection connection = null;
        PreparedStatement statement = null;
        try
        {
            connection = connectionPool.getConnection();
            String query = getFieldUpdateQuery(field);
            logger.info(EXECUTING_QUERY + query);
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage(), e);
        }
        finally
        {
            if (statement != null)
            {
                try
                {
                    statement.close();
                }
                catch (SQLException logOrIgnore)
                {
                    logger.error(logOrIgnore);
                }
            }
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch (SQLException logOrIgnore)
                {
                    logger.error(logOrIgnore);
                }
            }
        }
    }

    /**
     * Метод вставки T значения в таблицу
     *
     * @param value
     * @return
     * @throws SQLException
     */
    public int insert(T value) throws SQLException
    {
        int result = -1;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet returnSet = null;
        try
        {
            connection = connectionPool.getConnection();
            String query = getInsertQuery();
            logger.info(EXECUTING_QUERY + query);
            statement = connection.prepareStatement(getInsertQuery(), Statement.RETURN_GENERATED_KEYS);
            prepareInsertStatement(statement, value);
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0)
            {
                throw new SQLException(CREATING_USER_FAILED_NO_ROWS_AFFECTED);
            }
            returnSet = statement.getGeneratedKeys();
            if (returnSet.next() && returnSet != null)
            {
                result = returnSet.getInt(Constants.FieldConstants.ID_FIELD);
            }
        }
        finally
        {
            if (returnSet != null)
            {
                returnSet.close();
            }
            if (statement != null)
            {
                try
                {
                    statement.close();
                }
                catch (SQLException logOrIgnore)
                {
                    logger.error(logOrIgnore);
                }
            }
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch (SQLException logOrIgnore)
                {
                    logger.error(logOrIgnore);
                }
            }
        }
        return result;
    }

    /**
     * Выполняет update
     *
     * @param row
     * @throws SQLException
     */
    public void update(T row)
    {
        Connection connection = null;
        PreparedStatement statement = null;
        try
        {
            connection = connectionPool.getConnection();
            String query = getUpdateQuery();
            logger.info(EXECUTING_QUERY + query);
            statement = connection.prepareStatement(query);
            prepareUpdateStatement(statement, row);
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage(), e);
        }
        finally
        {
            if (statement != null)
            {
                try
                {
                    statement.close();
                }
                catch (SQLException logOrIgnore)
                {
                    logger.error(logOrIgnore);
                }
            }
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch (SQLException logOrIgnore)
                {
                    logger.error(logOrIgnore);
                }
            }
        }
    }

    protected abstract String getDeleteQuery();

    protected abstract String getFieldUpdateQuery(String field);

    protected abstract String getInsertQuery();

    protected abstract String getUpdateQuery();

    protected abstract void prepareDeleteStatement(int id, PreparedStatement statement) throws SQLException;

    protected abstract void prepareInsertStatement(PreparedStatement statement, T value) throws SQLException;

    protected abstract void prepareUpdateStatement(PreparedStatement statement, T row) throws SQLException;
}
