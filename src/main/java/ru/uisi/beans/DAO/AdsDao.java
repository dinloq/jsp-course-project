package ru.uisi.beans.DAO;

import org.springframework.stereotype.Component;
import ru.uisi.Constants;
import ru.uisi.beans.SqlQueryFormatter;
import ru.uisi.structs.Advertisement;

import javax.inject.Inject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import static ru.uisi.Constants.FieldConstants.*;

/**
 * Дао получения объявлений
 *
 * @author andrey
 * @since 10/15/13 - 10:27 PM
 */
@Component
public class AdsDao extends SimpleDao<Advertisement>
{
    @Inject
    CategoryDao categoryDao;
    @Inject
    CityDao cityDao;
    @Inject
    SqlQueryFormatter formatter;

    @Override
    protected void formatRows(ResultSet resultSet) throws SQLException
    {
        while (resultSet.next())
        {
            Advertisement row = getResultRow(resultSet);
            map.put(row.getId(), row);
        }
    }

    @Override
    protected String getDeleteQuery()
    {
        return formatter.deleteById(Constants.TablesConstants.ADS);
    }

    @Override
    protected String getFieldUpdateQuery(String field)
    {
        return formatter.incrementFieldQuery(Constants.TablesConstants.ADS, field);
    }

    @Override
    protected String getInsertQuery()
    {
        return formatter.insertAdvertisement();
    }

    @Override
    protected Advertisement getResultRow(ResultSet resultSet) throws SQLException
    {
        Advertisement row = new Advertisement();
        row.setAddress(resultSet.getString(ADDRESS));
        row.setCategory(resultSet.getInt(CAT));
        row.setCity(resultSet.getInt(CITY));
        row.setDate(resultSet.getDate(DATE));
        row.setEmail(resultSet.getString(EMAIL));
        row.setPhone(resultSet.getString(PHONE));
        row.setPrice(resultSet.getInt(PRICE));
        row.setText(resultSet.getString(TEXT_FIELD));
        row.setViews(resultSet.getInt(VIEWS));

        row.setName(resultSet.getString(NAME_FIELD));
        row.setId(resultSet.getInt(ID_FIELD));
        return row;
    }

    @Override
    protected String getUpdateQuery()
    {
        return formatter.updateAdvertisement();
    }

    @Override
    protected void prepareDeleteStatement(int id, PreparedStatement statement) throws SQLException
    {
        statement.setInt(1, id);
    }

    @Override
    protected void prepareInsertStatement(PreparedStatement statement, Advertisement value) throws SQLException
    {
        statement.setString(1, value.getName());
        statement.setInt(2, value.getCategory());
        statement.setString(3, value.getAddress());
        statement.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
        statement.setString(5, value.getPhone());
        statement.setInt(6, value.getPrice());
        statement.setString(7, value.getText());
        statement.setInt(8, value.getCity());
        statement.setString(9, value.getEmail());
    }

    @Override
    protected void prepareUpdateStatement(PreparedStatement statement, Advertisement row) throws SQLException
    {
        statement.setString(1, row.getName());
        statement.setInt(2, row.getCategory());
        statement.setString(3, row.getAddress());
        statement.setString(4, row.getPhone());
        statement.setInt(5, row.getPrice());
        statement.setString(6, row.getText());
        statement.setInt(7, row.getCity());
        statement.setInt(8, row.getViews());
        statement.setString(9, row.getEmail());
        statement.setInt(10, row.getId());
    }
}
