package ru.uisi.beans.DAO;

import com.google.common.collect.Maps;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import ru.uisi.beans.ConnectionPool;
import ru.uisi.structs.SimpleRow;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import static ru.uisi.Constants.LoggerConstants.EXECUTING_QUERY;

/**
 * @author andrey
 * @since 11/5/13 - 8:28 PM
 */
public abstract class SelectDao<T extends SimpleRow>
{
    private final static Log logger = LogFactory.getLog(SelectDao.class);
    protected Map<Integer, T> map;
    @Inject
    ConnectionPool connectionPool;

    public T getFirstResult(String query) throws SQLException
    {
        Map<Integer, T> map = getResultMap(query);
        return map.values().iterator().next();
    }

    /**
     * Метод получения по запросу из бд данных и формирование их в читабельный формат
     *
     * @param query
     * @return
     * @throws SQLException
     */
    public Map<Integer, T> getMap(String query) throws SQLException
    {
        return getResultMap(query);
    }

    /**
     * Формирует List<? extends SimpleRow> из ResultSet
     *
     * @param resultSet
     * @throws SQLException
     */
    protected abstract void formatRows(ResultSet resultSet) throws SQLException;

    private Map<Integer, T> getResultMap(String query) throws SQLException
    {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        map = Maps.newLinkedHashMap();
        try
        {
            connection = connectionPool.getConnection();
            statement = connection.createStatement();
            logger.info(EXECUTING_QUERY + query);
            resultSet = statement.executeQuery(query);
            formatRows(resultSet);
        }
        finally
        {
            if (resultSet != null)
            {
                try
                {
                    resultSet.close();
                }
                catch (SQLException logOrIgnore)
                {
                    logger.error(logOrIgnore);
                }
            }
            if (statement != null)
            {
                try
                {
                    statement.close();
                }
                catch (SQLException logOrIgnore)
                {
                    logger.error(logOrIgnore);
                }
            }
            if (connection != null)
            {
                try
                {
                    connection.close();
                }
                catch (SQLException logOrIgnore)
                {
                    logger.error(logOrIgnore);
                }
            }
        }
        return map;
    }

    abstract protected T getResultRow(ResultSet set) throws SQLException;
}
