package ru.uisi.beans;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static ru.uisi.Constants.ConnectorConstants.*;

/**
 * Пул соединений для их повторного использования
 * Исключает повторное создание соединений,
 * используя существующие, находящиеся в пуле
 *
 * @author andrey
 * @since 10/11/13 - 8:09 PM
 */

@Component
public class ConnectionPool
{
    public static final Log logger = LogFactory.getLog(ConnectionPool.class);

    public Connection getConnection()
    {
        return initConnection();
    }

    private Connection initConnection()
    {
        try
        {
            Class.forName(ORG_POSTGRESQL_DRIVER);
            return DriverManager.getConnection(DB_URL, USER, PASSWORD);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            logger.error(e.getMessage(), e);
        }
        return null;
    }
}
