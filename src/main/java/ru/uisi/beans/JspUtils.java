package ru.uisi.beans;

import com.google.common.collect.Maps;
import org.springframework.stereotype.Component;
import ru.uisi.beans.DAO.CategoryDao;
import ru.uisi.beans.DAO.CityDao;
import ru.uisi.structs.Category;
import ru.uisi.structs.City;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.Map;

import static java.util.Map.Entry;
import static ru.uisi.Constants.TablesConstants.CATEGORIES;
import static ru.uisi.Constants.TablesConstants.CITIES;

/**
 * Некоторые утилиты для работы приложения
 *
 * @author andrey
 * @since 11/3/13 - 2:57 PM
 */
@Component
public class JspUtils
{
    @Inject
    private CategoryDao categoryDao;
    @Inject
    private CityDao cityDao;
    @Inject
    private SqlQueryFormatter formatter;

    /**
     * Получает Map<K, V>, где
     * K - id категории
     * V - название категории
     *
     * @return
     */
    public Map getMapCategories()
    {
        Map<Integer, String> categories = Maps.newLinkedHashMap();
        String query = formatter.select(CATEGORIES);
        try
        {
            for (Entry<Integer, Category> entry : categoryDao.getMap(query).entrySet())
            {
                categories.put(entry.getKey(), entry.getValue().getName());
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return categories;
    }

    /**
     * Получает Map<K, V>, где
     * K - id города
     * V - имя города
     *
     * @return
     */
    public Map getMapCities()
    {
        Map<Integer, String> cities = Maps.newLinkedHashMap();
        String query = formatter.select(CITIES);
        try
        {
            for (Entry<Integer, City> entry : cityDao.getMap(query).entrySet())
            {
                cities.put(entry.getKey(), entry.getValue().getName());
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return cities;
    }
}
