<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="${request.contextPath}"/>
    <title></title>
</head>
<body>
<div class="categories-table">
    <table style="margin: auto">
        <tr>


            <td style="width: 200px; overflow: hidden">
                <a href="/index">

                    <div class="category-cell">
                        <p class="category-title">
                            <strong>
                                Все
                            </strong>
                        </p>
                    </div>
                </a>
            </td>


            <c:forEach var="item" items="${categories}" varStatus="loop">
                <td style="width: 200px; overflow: hidden">
                    <a href="/index?cat=${item.id}">

                        <div class=" category-cell">
                            <p class="category-title">
                                <strong>
                                    <c:out value="${item.name}"/>
                                </strong>
                            </p>
                        </div>
                    </a>
                </td>
            </c:forEach>
        </tr>
    </table>
</div>
</body>
</html>