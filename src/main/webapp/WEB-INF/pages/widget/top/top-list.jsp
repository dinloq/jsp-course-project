<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/resources/styles/top-list.css" type="text/css"/>
</head>
<body>
<div class="top-list-border">
    <h2><strong>Успевай, пока не купили!</strong></h2>
    </br>
    <c:forEach var="topitem" items="${tops}" varStatus="loop">
        <c:set var="top" scope="request" value="${topitem}"/>
        <jsp:include page="top-item.jsp"/>
    </c:forEach>
</div>
</body>
</html>