<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <link rel="stylesheet" href="/resources/styles/sidebar.css" type="text/css"/>
    <title></title>
</head>
<body>
<div class="sidebar" id="sideRight">

    <table style="width: 100%">
        <tr>
            <td>
                <div class="login-button">
                    <a href="<c:url value="/admin/list" />">Вход</a></br>
                </div>
            </td>
            <td>
                <div class="login-button">
                    <a href="<c:url value="/j_spring_security_logout" />">Выход</a>
                </div>
            </td>
        </tr>
    </table>

    <a href="/add-item">
        <div class="add-ad-button"><h3 class="no-a-text" style="margin-top: 7px; margin-bottom: 7px">Подать
            объявление</h3></div>
    </a>

    <div id="sidebar-cont">
        <jsp:include page="//top-list"/>
    </div>
</div>
<!-- .sidebar#sideRight -->
</body>
</html>