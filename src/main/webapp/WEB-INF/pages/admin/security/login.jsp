<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Вход</title>
    <link rel="stylesheet" href="/resources/styles/login.css" type="text/css"/>
</head>
<body onload='document.f.j_username.focus();'>
<div class="login-main">
    <div class="login-border">

        <h3>Войти под учетной записью</h3>

        <c:if test="${not empty error}">
            <div class="errorblock">
                Вы не смогли войти в систему, попробуйте еще раз.<br/> Причина :
                    ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
            </div>
        </c:if>

        <form name='f' action="<c:url value='/j_spring_security_check' />"
              method='POST'>

            <table style="margin: auto;">
                <tr>
                    <td>Логин:</td>
                    <td><input type='text' name='j_username' value=''>
                    </td>
                </tr>
                <tr>
                    <td>Пароль:</td>
                    <td><input type='password' name='j_password'/>
                    </td>
                </tr>
                <tr style="text-align: center">
                    <td colspan='2'><input name="submit" type="submit"/>
                    </td>
                </tr>
                <tr style="text-align: center">
                    <td colspan='2'><input name="reset" type="reset"/>
                    </td>
                </tr>
            </table>

        </form>
    </div>
</div>
</body>
</html>