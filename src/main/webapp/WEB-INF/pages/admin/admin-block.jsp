<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/resources/styles/admin-block.css" type="text/css"/>
</head>
<body>
<div class="block-border">
    <table class="fill-parent">
        <tr>
            <td class="pic-td"><h3>Фото</h3></td>
            <td>
                <table class="fill-parent">
                    <tr class="block-title">
                        <td><h2 style="padding-left: 20px;"><a href="/admin/view?id=${ad.id}"><c:out
                                value="${ad.name}"/></a></h2></td>
                        <td class="delete-link"><a href="/admin/delete?id=${ad.id}" class="delete-link">[ X ]</a>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 35%">Категория:</td>
                        <td>${categories[ad.category].name}</td>
                    </tr>
                    <tr>
                        <td>Город:</td>
                        <td>${cities[ad.city].name}</td>
                    </tr>
                    <tr>
                        <td>Цена:</td>
                        <td>${ad.price} руб.</td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td>${ad.email}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="date-td">${ad.date}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>