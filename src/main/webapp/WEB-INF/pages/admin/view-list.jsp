<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <title></title>
</head>
<body>
<c:forEach var="ad" items="${ads}" varStatus="loop">
    <c:set var="ad" scope="request" value="${ad}"/>
    <jsp:include page="admin-block.jsp" flush="true"/>
</c:forEach>

</body>
</html>