<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <link rel="stylesheet" href="/resources/styles/main.css" type="text/css"/>
</head>

<body>
<div class="background"></div>

<div id="wrapper">
    <div id="cont">
        <div id="inset-cont">
            <div id="main-page-cont">
                <jsp:include page="widget/header.jsp" flush="true"/>
                <jsp:include page="middle.jsp" flush="true"/>
                <jsp:include page="widget/footer.jsp" flush="true"/>
            </div>
        </div>
    </div>
</div>
<!-- #wrapper -->

</body>
</html>
