<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <title></title>
</head>
<body>
<div id="middle">
    <table>
        <tr>
            <td style="vertical-align: top;">
                <div id="container">
                    <div id="content">
                        <jsp:include page="//categories-panel"/>

                        <c:choose>
                            <c:when test="${mode eq 'list'}">
                                <jsp:include page="item/content.jsp"/>
                            </c:when>

                            <c:when test="${mode eq 'view'}">
                                <jsp:include page="item/view-item.jsp" flush="true"/>
                            </c:when>

                            <c:when test="${mode eq 'add'}">
                                <jsp:include page="item/add-item.jsp"/>
                            </c:when>

                            <c:when test="${mode eq 'adminList'}">
                                <jsp:include page="admin/view-list.jsp"/>
                            </c:when>

                            <c:when test="${mode eq 'adminUpdate'}">
                                <jsp:include page="admin/view-item.jsp"/>
                            </c:when>
                            <c:otherwise>error</c:otherwise>
                        </c:choose>
                    </div>
                    <!-- #content-->

                </div>
                <!-- #container-->
            </td>
            <td style="vertical-align: top;">
                <jsp:include page="widget/sidebar.jsp"/>
            </td>
        </tr>
    </table>
</div>
<!-- #middle-->
</body>
</html>