<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <link rel="stylesheet" href="/resources/styles/view.css" type="text/css"/>
</head>
<body>

<div id="ad-border">
    <div class="ad-content">

        <h1 class="ad-title"><c:out value="${ad.name}"/></h1>
        <h4 class="ad-date"><c:out value="${ad.date}"/></h4>

        <div class="ad-pic"></div>

        <p>

        <div class="ad-price">
            <span class="title-column">Цена:</span><b class="price-column"><c:out value="${ad.price}"/> руб.</b>
        </div>
        </p>

        <div class="ad-delimiter"></div>

        <p>

        <div class="ad-seller">
            <span class="title-column">Продавец:</span><span class="seller-column"><c:out value="${ad.email}"/></span>
        </div>
        </p>

        <p>

        <div class="ad-seller">
            <span class="title-column">Телефон:</span><span class="seller-column"><c:out value="${ad.phone}"/></span>
        </div>
        </p>

        <p>

        <div class="ad-seller">
            <span class="title-column">Город:</span><span class="seller-column"><c:out value="${city.name}"/></span>
        </div>
        </p>

        <div class="ad-delimiter"></div>

        <p>

        <div class="ad-seller">
            <span class="title-column">Категория:</span><span class="seller-column"><c:out
                value="${category.name}"/></span>
        </div>
        </p>

        <p>

        <div class="ad-seller">
            <b>
                <div class="title-column">Описание:</div>
            </b>

            <p class="ad-description"><c:out value="${ad.text}"/></p>
        </div>
        </p>

        <div class="ad-id">
            <span class="title-column">Номер объявления: </span><span class="title-column">${ad.id}</span>
        </div>

    </div>
</div>
</body>
</html>