<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/resources/styles/main.css" type="text/css"/>
</head>
<body>
<c:forEach var="ad" items="${ads}" varStatus="loop">
    <c:set var="ad" scope="request" value="${ad}"/>
    <jsp:include page="block.jsp" flush="true"/>
</c:forEach>
<div class="paging-div">
    <c:if test="${pageIndex > 1}">
        <a href="/index?cat=${cat}&page=${pageIndex - 1}">
            <span class="paging-element">Предыдущая</span>
        </a>
    </c:if>
    <span class="paging-element">Страница <c:out value="${pageIndex}"/></span>
    <c:if test="${hasNextPage eq true}">
        <a href="/index?cat=${cat}&page=${pageIndex + 1}">
            <span class="paging-element">Следующая</span>
        </a>
    </c:if>
</div>

</body>
</html>