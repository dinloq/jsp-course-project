<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/resources/styles/add-item.css" type="text/css"/>
</head>
<body>
<form:form action="add-item" commandName="advertisement" method="post">
    <table class="table-style">
        <tr class="row-class" style="height: 40px">
            <td colspan="2" style="text-align: center"><h2>Создание объявления</h2></td>
        </tr>

        <tr class="row-class">
            <td colspan="2"><form:errors path="*" cssClass="error"/></td>
        </tr>
        <tr class="row-class">
            <td style="width: 25%"><h3>Заголовок :</h3></td>
            <td><form:input path="name" cssClass="input-element"/></td>
        </tr>

        <tr class="row-class">
            <td><h3>Категрория :</h3></td>
            <td><form:select path="category" items="${categories}" cssClass="input-element"/></td>
        </tr>

        <tr class="row-class">
            <td><h3>Город :</h3></td>
            <td><form:select path="city" items="${cities}" cssClass="input-element"/></td>
        </tr>

        <tr class="row-class">
            <td><h3>Адресс :</h3></td>
            <td><form:input path="address" cssClass="input-element"/></td>
        </tr>

        <tr class="row-class">
            <td><h3>Цена :</h3></td>
            <td><form:input path="price" cssClass="input-element"/></td>
        </tr>

        <tr class="row-class">
            <td><h3>Телефон :</h3></td>
            <td><form:input path="phone" cssClass="input-element"/></td>
        </tr>


        <tr class="row-class">
            <td><h3>Email :</h3></td>
            <td><form:input path="email" cssClass="input-element"/></td>
        </tr>

        <tr class="row-class">
            <td><h3>Текст объявления:</h3></td>
            <td><form:textarea path="text" cssClass="input-element" rows="15"/></td>
        </tr>

        <tr class="row-class" style="text-align: center">
            <td colspan="2"><input type="submit" value="Подать объявление" class="button-class"/></td>
        </tr>
    </table>
</form:form>

</body>
</html>