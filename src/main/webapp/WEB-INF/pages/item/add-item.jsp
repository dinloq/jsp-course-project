<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="/resources/styles/add-item.css" type="text/css"/>
</head>
<body>

<div class="add-border">

    <c:choose>
        <c:when test="${addItemStatus eq 'progress'}">
            <jsp:include page="add-item-form.jsp"/>
        </c:when>
        <c:when test="${addItemStatus eq 'success'}">
            <jsp:include page="add-item-success.jsp"/>
        </c:when>
    </c:choose>

</div>

</body>
</html>