<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <title></title>
</head>
<body>
<p>
    Объявление успешно обновлено. Желаете взглянуть? <br>
    <a href="/view?id=${addedItem}">Переход</a>
</p>
</body>
</html>