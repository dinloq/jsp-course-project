<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="/resources/styles/block.css" type="text/css"/>
</head>
<body>
<div class="block-border">
    <table class="fill-parent">
        <tr>
            <td class="pic-td"><h3>Фото</h3></td>
            <td>
                <table class="fill-parent">
                    <tr>
                        <td><h2><a href="/view?id=${ad.id}"><c:out value="${ad.name}"/></a></h2></td>
                    </tr>
                    <tr>
                        <td><h3><c:out value="${ad.price}"/> руб.</h3></td>
                    </tr>
                    <tr>
                        <td><p class="type-style">${categories[ad.category].name}</p></td>
                    </tr>
                    <tr class="date-td">
                        <td><span><c:out value="${ad.date}"/></span></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>